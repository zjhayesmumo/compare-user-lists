package com.company;
// Version 1.01
import java.io.*;
import java.util.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class App
{
    static String list1;
    static String list2;
    static String outputName;

    static List<String> matchesArr;
    static List<String> list2Arr = new ArrayList<String>();

    public static void main(String[] args) throws IOException
    {
        /*if(args.length < 2)
        {
            System.out.println("Missing arguments.");
        }
        else
        {*/
            list1 = "alm_migated_projects_users_20190507.xlsx";
            list2 = "rh_pharm_ppe.xlsx";
            list2Arr = readFile(list2);
            generateOutputName();
            matchesArr = compareFile(list1);
            createCSV();
        //}
    }

    private static List<String> readFile(String list) throws IOException
    {
        List<String> listArr = new ArrayList<String>();

        FileInputStream file = new FileInputStream(new File(list));

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();

        while(rowIterator.hasNext())
        {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();

            while(cellIterator.hasNext())
            {
                Cell cell = cellIterator.next();

                String value = cell.getStringCellValue();

                listArr.add(value.trim());

            }
        }

        file.close();

        return listArr;
    }

    private static List<String> compareFile(String list) throws IOException
    {
        List<String> listArr = new ArrayList<String>();

        FileInputStream file = new FileInputStream(new File(list));

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();

        while(rowIterator.hasNext())
        {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();

            String username = cellIterator.next().getStringCellValue();

            if(list2Arr.contains(username))
            {
                listArr.add(username + "%" + cellIterator.next().getStringCellValue().trim() +
                "%" + cellIterator.next().getStringCellValue().trim());
            }
        }

        file.close();

        return listArr;
    }

    private static void createExcel() throws IOException
    {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Matching Users");
        int rownum = 0;

        for(String str : matchesArr)
        {
            Row row = sheet.createRow(rownum++);
            String[] strArr = str.split("%");
            int cellnum = 0;

            for(String cellValue : strArr)
            {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(cellValue);
            }
        }

        FileOutputStream out = new FileOutputStream(new File("matching_users.xlsx"));
        workbook.write(out);
        out.close();
        System.out.println("Matching usernames were successfully written to matching_users.xlsx");

    }

    private static void createCSV() throws IOException
    {
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputName));
        writer.write("USER_NAME,FULL_NAME,EMAIL\n");

        for(String str : matchesArr)
        {
            String[] strArr = str.split("%");

            writer.write(strArr[0] + "," + strArr[1] + "," + strArr[2] + "\n");
        }
        writer.close();
    }

    private static void generateOutputName()
    {
        String[] splitName = list2.split("\\.");
        outputName = splitName[0] + ".csv";
    }
}

